import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Andrii.
 */

public class Chess {
    
    final static int DIMANTION = 4; //0-x, 1-y, 2-figures, 3-attack counter

    final static int EMPTY = 0;
    final static int ON_FIRE = 1;
    final static int KING = 2;
    final static int ROOK = 3;
    final static int KNIGHT = 4;
    
    final static int OUT = -1;
    
    final static int width = 5;
    final static int hight = 5;

    static int[][] table;
    
    static int[][] figures = { // first - figura type, second - position in table
            {KING,OUT},
            {KING,OUT},
            {KNIGHT,OUT},
            {ROOK,OUT},
            {ROOK,OUT},
    };

    static ArrayList<int[][]> results = new ArrayList<int[][]>();
    
    public static void main(String[] args) {

        //---------------------------
        
        prepareTable(width,hight);
        //----------------------------

        int i = figures.length - 1;
        
        while (i < figures.length) {
            System.out.println("take from pool figure - " + i);
            boolean result = nextPut(i); //move to next possible posstion
            if (i == 0 && result) addResult(table);
            System.out.println("nextPut - " + result);
            if ( !result ) {
                System.out.println("###cant put figure - " + i + " - " + figures[i][0]);
                System.out.println("###UNput figure - " + i);
                unPut(i); //remove from the table set up OUT
                i++;
//                System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//                results.add(Arrays.copyOf(table, table.length));
                print(table);
            } else {
                System.out.println("put figure - " + i + " - " + figures[i][0]);
                if (i > 0) {
                    i--;
                    print(table);
                } else {
//                    results.add(table.clone());
                    print(table);
                }
            }
        }

        System.out.println("=================RESULT========================");
        for (int j = 0; j < results.size(); j++) {
            printRes(results.get(j));
            System.out.println("-------------------");
        }
        System.out.println("Total count is: " + results.size());
    }
    
    /*
    Take figure and put it to nex position if it possible and return true if impossible
    return false
     */
    private static boolean nextPut(int number) { //return true if can put in table return false if cant
        boolean result = false;
        if (figures[number][1] == OUT) { // figure out the table

            for (int i=0; i < table.length; i++) {
                if (table[i][2] == EMPTY && table[i][3] == 0 && put(i, number)) {
                    table[i][2] = figures[number][0];
                    figures[number][1] = i;
                    result = true;
                    break;
                }
            }

        } else { // figure on table
            System.out.println("====UNPUT NUMBER "+number+"===");
            int from = unPut(number);
            print(table);
            for (int i=from + 1; i < table.length; i++) {
                if (table[i][2] == EMPTY && table[i][3] == 0 && put(i, number)) {
                    table[i][2] = figures[number][0];
                    figures[number][1] = i;
                    result = true;
                    break;
                }
            }

        }

        return result;
    }

    private static boolean put(int position, int figure) {
        boolean result = false;
        if (figures[figure][0] == KING) {
            result = putKing(position, figure);    
        } else if (figures[figure][0] == ROOK) {
            result = putRook(position, figure);
        } else if (figures[figure][0] == KNIGHT) {
            result = putKnight(position, figure);
        } else {
            System.out.println("WTF???");
        }
        return result;
    }
    
    private static boolean checkPosition(int number) {
        System.out.println("+++++++++++" + number);
        if (table[number][2]==KING || table[number][2]==ROOK || table[number][2]==KNIGHT) {
            return false;
        }
        return true;
    }

    private static boolean checkPosition(int... numbers) {
        for (int i=0; i<numbers.length; i++) {
            if (numbers[i] != -1 && !checkPosition(numbers[i])) return false;
        }
        return true;
    }
    
    private static void onFire(int number) {
        table[number][3] += ON_FIRE;
    }

    private static void onFire(int... numbers) {
        for (int i=0; i<numbers.length; i++) {
            if (numbers[i] != -1)
                onFire(numbers[i]);
        }
    }

    private static void outFire(int number) {
        if (table[number][3] == 0) {
            System.out.println("*********outFire error************");
            System.out.println(number);
            print(table);
            System.out.println("************************************");
            return;
        }
            
        table[number][3] -= ON_FIRE;
    }

    private static void outFire(int... numbers) {
        for (int i=0; i<numbers.length; i++) {
            if (numbers[i] != -1)
                outFire(numbers[i]);
        }
    }
    
    private static int getNumberByXY(int x, int y) {
        return width * x + y;
    }
    
    private static boolean putKing(int position, int figure) {
        boolean result = false;
        int x = table[position][0];
        int y = table[position][1];
        
        
        int[] points = {-1, -1, -1, -1, -1, -1, -1, -1};
        
        if (x-1 >= 0 && y-1 >= 0) points[0] = width*(x-1) + (y-1);
        if (x   >= 0 && y-1 >=0)   points[1] = width*(x)   + (y-1);
        if (x+1 < width && y-1 >=0)  points[2] = width*(x+1) + (y-1);
        if (x+1 < width && y>=0)    points[3] = width*(x+1) + (y);
        if (x-1 >= 0 && y+1< hight)  points[4] = width*(x-1) + (y+1);
        if (x-1 >= 0 && y>=0)    points[5] = width*(x-1) + (y);
        if (x >= 0 && y+1< hight)  points[6] = width*(x) + (y+1);
        if (x+1 <width && y+1<hight)  points[7] = width*(x+1) + (y+1);

        System.out.println("**********************");
        System.out.println(x + " " + y);
        print(points);
        System.out.println("*************************");

        if (checkPosition(points)) {
            result = true;
            onFire(points);
        } else {
            System.out.println("cant put figure in position - " + x + ", " + y);
            print(table);
        }
        
        return result;
    }

    private static boolean putRook(int position, int figure) {
        boolean result = false;
        int x = table[position][0];
        int y = table[position][1];
        
        int[] xs = new int[width - 1];
        int[] ys = new int[hight - 1];
        
        //----------------------X variable, Y fixed---------------------
        for (int i = 0; i < x; i++) {
            xs[i] = getNumberByXY(i, y);
        }

        for (int i = x + 1; i < width; i++) {
            xs[i-1] = getNumberByXY(i, y);
        }

        //-----------------------Y variable, X fixed--------------------
        for (int j = 0; j < y; j++) {
            ys[j] = getNumberByXY(x, j);
        }

        for (int j = y + 1; j < width; j++) {
            ys[j-1] = getNumberByXY(x, j);
        }
        System.out.println("AAAAAAAAAAAAAAAAAAA");
        print(xs);
        print(ys);
        System.out.println("AAAAAAAAAAAAAAAAAAA");
        if (checkPosition(xs) && checkPosition(ys)) {
            result = true;
            onFire(xs);
            onFire(ys);
        }
        
        return result;
    }

    private static boolean putKnight(int position, int figure) {
        boolean result = false;
        int x = table[position][0];
        int y = table[position][1];


        int[] points = {-1, -1, -1, -1, -1, -1, -1, -1};

        if (x+1 < width && y-2 >= 0) points[0] = width*(x+1) + (y-2);
        if (x+2 < width && y-1 >=0)   points[1] = width*(x+2)   + (y-1);
        if (x+2 < width && y+1 < hight)  points[2] = width*(x+2) + (y+1);
        if (x+1 < width && y+2 < hight)    points[3] = width*(x+1) + (y+2);
        if (x-1 >= 0 && y+2 < hight)  points[4] = width*(x-1) + (y+2);
        if (x-2 >= 0 && y+1 < hight)    points[5] = width*(x-2) + (y+1);
        if (x-2 >= 0 && y-1 >= 0)  points[6] = width*(x-2) + (y-1);
        if (x-1 >= 0  && y-2 >=0)  points[7] = width*(x-1) + (y-2);

        System.out.println("**********************");
        System.out.println(x + " " + y);
        print(points);
        System.out.println("*************************");

        if (checkPosition(points)) {
            result = true;
            onFire(points);
        } else {
            System.out.println("cant put figure in position - " + x + ", " + y);
            print(table);
        }

        return result;

    }

    /*
    return -1 if figure out or return position
     */
    private static int unPut(int figure) {
        if (figures[figure][1] == OUT) {
            return -1;
        }

        if (figures[figure][0] == KING) {
            unPutKing(figures[figure][1]);
        } else if (figures[figure][0] == ROOK) {
            unPutRook(figures[figure][1]);
        } else if (figures[figure][0] == KNIGHT) {
            unPutKnight(figures[figure][1]);
        } else {
            System.out.println("WTF???");
        }

        System.out.println("*************************" + figure);
        for (int i = 0; i < figures.length; i++) {
            for (int j = 0; j < 2; j++) {
                System.out.print(figures[i][j]);
            }
            System.out.println();
        }

        table[figures[figure][1]][2]=EMPTY;
        int result = figures[figure][1];
        figures[figure][1] = OUT;
        return result;
    }

    private static boolean unPutKing(int position) {
        if (table[position][2] != KING)
            System.out.println("ERRRORRRR!!!!!");

        boolean result = false; // TODO unusible, remove in the feature

        int x = table[position][0];
        int y = table[position][1];


        int[] points = {-1, -1, -1, -1, -1, -1, -1, -1};

        if (x-1 >= 0 && y-1 >= 0) points[0] = width*(x-1) + (y-1);
        if (x   >= 0 && y-1 >=0)   points[1] = width*(x)   + (y-1);
        if (x+1 < width && y-1 >=0)  points[2] = width*(x+1) + (y-1);
        if (x+1 < width && y>=0)    points[3] = width*(x+1) + (y);
        if (x-1 >= 0 && y+1< hight)  points[4] = width*(x-1) + (y+1);
        if (x-1 >= 0 && y>=0)    points[5] = width*(x-1) + (y);
        if (x >= 0 && y+1< hight)  points[6] = width*(x) + (y+1);
        if (x+1 <width && y+1<hight)  points[7] = width*(x+1) + (y+1);

        print(points);

        outFire(points);

        return true;
    }

    private static boolean unPutRook(int position) {
        boolean result = false;
        int x = table[position][0];
        int y = table[position][1];

        int[] xs = new int[width - 1];
        int[] ys = new int[hight - 1];

        //----------------------X variable, Y fixed---------------------
        for (int i = 0; i < x; i++) {
            xs[i] = getNumberByXY(i, y);
        }

        for (int i = x + 1; i < width; i++) {
            xs[i-1] = getNumberByXY(i, y);
        }

        //-----------------------Y variable, X fixed--------------------
        for (int j = 0; j < y; j++) {
            ys[j] = getNumberByXY(x, j);
        }

        for (int j = y + 1; j < width; j++) {
            ys[j-1] = getNumberByXY(x, j);
        }

        outFire(xs);
        outFire(ys);

        return result;

    }

    private static boolean unPutKnight(int position) {
        if (table[position][2] != KING)
            System.out.println("ERRRORRRR!!!!!");

        boolean result = false; // TODO unusible, remove in the feature

        int x = table[position][0];
        int y = table[position][1];


        int[] points = {-1, -1, -1, -1, -1, -1, -1, -1};

        if (x+1 < width && y-2 >= 0) points[0] = width*(x+1) + (y-2);
        if (x+2 < width && y-1 >=0)   points[1] = width*(x+2)   + (y-1);
        if (x+2 < width && y+1 < hight)  points[2] = width*(x+2) + (y+1);
        if (x+1 < width && y+2 < hight)    points[3] = width*(x+1) + (y+2);
        if (x-1 >= 0 && y+2 < hight)  points[4] = width*(x-1) + (y+2);
        if (x-2 >= 0 && y+1 < hight)    points[5] = width*(x-2) + (y+1);
        if (x-2 >= 0 && y-1 >= 0)  points[6] = width*(x-2) + (y-1);
        if (x-1 >= 0  && y-2 >=0)  points[7] = width*(x-1) + (y-2);

        print(points);

        outFire(points);

        return true;
    }
    
    
    
    private static void prepareTable(int width, int hight) {
        table = new int[width*hight][DIMANTION];

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < hight; j++) {
                table[i*width + j][0] = i;
                table[i*width + j][1] = j;
                table[i*width + j][2] = EMPTY;
            }
        }
    }

    private static void printRes(int[][] res) {
        int[][] nativeTable = new int[width][hight];

        for (int i = 0; i < res.length; i++) {
            nativeTable[res[i][0]][res[i][1]] = res[i][2];
        }

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < hight; j++) {
                System.out.print(nativeTable[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static void print(int[][] table) {
        int[][] nativeTable = new int[width][hight];

        for (int i = 0; i < table.length; i++) {
            nativeTable[table[i][0]][table[i][1]] = table[i][2];    
        }

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < hight; j++) {
                System.out.print(nativeTable[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("+++++++");
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[0].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("--------------------------------------------------");
    }

    private static  void print(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    private static void addResult(int[][] res) {
        for (int i = 0; i < results.size(); i++) {
            if (isEqual(results.get(i), res)) return;
        }

        int[][] copyRes = new int[res.length][res[0].length];
        for (int i = 0; i < res.length; i++) {
            for (int j = 0; j < res[0].length; j++) {
                copyRes[i][j] = res[i][j];
            }
        }
        results.add(copyRes);
    }

    private static boolean isEqual(int[][] first, int[][] second) {
        boolean result = true;
        for (int i = 0; i < first.length; i++) {
            if (first[i][2] != second[i][2]) {
                result = false;
                break;
            }
        }

        return result;
    }
}
