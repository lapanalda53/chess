import java.util.Scanner;

/**
 * Created by lapanalda on 13.06.15.
 */
public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        System.out.println("Input M dimension of chess board:");
//        int mDimension = scanner.nextInt();
//        System.out.println("Input N dimension of chess board:");
//        int nDimension = scanner.nextInt();
//        System.out.println("Input the count of kings");
//        int kingsCount = scanner.nextInt();
//        System.out.println("Input the count of rooks");
//        int rooksCount = scanner.nextInt();
//        System.out.println("Input the count of knights");
//        int knightsCount = scanner.nextInt();
//        Board board = new Board(mDimension, nDimension);
        Board board = new Board(5, 5);
        Chess chess = new Chess(board, 2, 2, 1);
    //    Chess chess = new Chess(board, kingsCount, rooksCount, knightsCount);
        chess.countResultSets();
    }
}
