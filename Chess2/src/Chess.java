import java.util.*;

/**
 * Created by lapanalda on 13.06.15.
 */
public class Chess {

    public static final byte FULL_PEACE = 5;
    public static final String EMPTY_SIGN = ".";
    public static final String KING = "K";
    public static final String ROOK = "R";
    public static final String KNIGHT = "N";
    public static final byte KING_INT = 1;
    public static final byte ROOK_INT = 2;
    public static final byte KNIGHT_INT = 3;

    private Board board;
    private byte[] figures;
    private String[] figuresString;

    private List<String[][]> resultSet;
    private List<byte[]> figuresCombinations = new ArrayList<>();

    public Chess(Board board, int kingCount, int rookCount, int knightCount) {
        this.board = board;
        this.figures = new byte[kingCount + rookCount + knightCount];
        this.createFiguresList(kingCount, rookCount);
    }

    private void createFiguresList(int kingCount, int rookCount) {
        figuresString = new String[figures.length];
        for (int i = 0; i < figures.length; i++) {
            if (i < kingCount) {
                figures[i] = KING_INT;
                figuresString[i] = KING;
            } else if (i >= kingCount && i < kingCount + rookCount) {
                figures[i] = ROOK_INT;
                figuresString[i] = ROOK;
            } else {
                figures[i] = KNIGHT_INT;
                figuresString[i] = KNIGHT;
            }
        }
    }

    private boolean findEquals(List<byte[]> combinations, byte[] combination) {
        boolean result = false;
        for (byte[] array : combinations) {
            if (Arrays.equals(array, combination)) {
                result = true;
            }
        }
        return result;
    }

    public void permuteFigures(byte[] list, int startIndex) {
        if (startIndex + 1 == list.length) {
            byte[] combination = new byte[list.length];
            System.arraycopy(list, 0, combination, 0, list.length);
            if (!findEquals(figuresCombinations, combination)) {
                figuresCombinations.add(combination);
            }
        } else {
            for (int i = startIndex; i < list.length; i++) {
                byte temp = list[i];
                list[i] = list[startIndex];
                list[startIndex] = temp;
                permuteFigures(list, startIndex + 1);
            }
        }
    }

    private void setCellValue(int i, int j) {
        if (board.getTable()[i][j] == Board.DEFAULT_SIGN) {
            board.getTable()[i][j] = FULL_PEACE;
        } else if (board.getTable()[i][j] > FULL_PEACE || board.getTable()[i][j] == FULL_PEACE) {
            board.getTable()[i][j]++;
        }
    }

    private boolean checkCellValue(int i, int j) {
        if (board.getTable()[i][j] == Board.DEFAULT_SIGN) {
            return true;
        } else if (board.getTable()[i][j] > FULL_PEACE || board.getTable()[i][j] == FULL_PEACE) {
            return true;
        }
        return false;
    }

    private void clearCell(int i, int j) {
        if (board.getTable()[i][j] == FULL_PEACE) {
            board.getTable()[i][j] = Board.DEFAULT_SIGN;
        } else if (board.getTable()[i][j] > FULL_PEACE) {
            board.getTable()[i][j]--;
        }
    }

    private boolean checkSetKing(int i, int j) {
        if (i + 1 < board.getM()) {
            if (!checkCellValue(i + 1, j)) {
                return false;
            }
        }
        if (i - 1 >= 0) {
            if (!checkCellValue(i - 1, j)) {
                return false;
            }
        }
        if (j + 1 < board.getN()) {
            if (!checkCellValue(i, j + 1)) {
                return false;
            }
        }
        if (j - 1 >= 0) {
            if (!checkCellValue(i, j - 1)) {
                return false;
            }
        }
        if (i + 1 < board.getM() && j + 1 < board.getN()) {
            if (!checkCellValue(i + 1, j + 1)) {
                return false;
            }
        }
        if (i + 1 < board.getM() && j - 1 >= 0) {
            if (!checkCellValue(i + 1, j - 1)) {
                return false;
            }
        }
        if (i - 1 >= 0 && j + 1 < board.getN()) {
            if (!checkCellValue(i - 1, j + 1)) {
                return false;
            }
        }
        if (i - 1 >= 0 && j - 1 >= 0) {
            if (!checkCellValue(i - 1, j - 1)) {
                return false;
            }
        }
        return true;
    }

    private void setKing(int i, int j) {
        board.getTable()[i][j] = KING_INT;
        if (i + 1 < board.getM()) {
            setCellValue(i + 1, j);
        }
        if (i - 1 >= 0) {
            setCellValue(i - 1, j);
        }
        if (j + 1 < board.getN()) {
            setCellValue(i, j + 1);
        }
        if (j - 1 >= 0) {
            setCellValue(i, j - 1);
        }
        if (i + 1 < board.getM() && j + 1 < board.getN()) {
            setCellValue(i + 1, j + 1);
        }
        if (i + 1 < board.getM() && j - 1 >= 0) {
            setCellValue(i + 1, j - 1);
        }
        if (i - 1 >= 0 && j + 1 < board.getN()) {
            setCellValue(i - 1, j + 1);
        }
        if (i - 1 >= 0 && j - 1 >= 0) {
            setCellValue(i - 1, j - 1);
        }
    }

    private void removeKing(int i, int j) {
        board.getTable()[i][j] = Board.DEFAULT_SIGN;
        if (i + 1 < board.getM()) {
            clearCell(i + 1, j);
        }
        if (i - 1 >= 0) {
            clearCell(i - 1, j);
        }
        if (j + 1 < board.getN()) {
            clearCell(i, j + 1);
        }
        if (j - 1 >= 0) {
            clearCell(i, j - 1);
        }
        if (i + 1 < board.getM() && j + 1 < board.getN()) {
            clearCell(i + 1, j + 1);
        }
        if (i + 1 < board.getM() && j - 1 >= 0) {
            clearCell(i + 1, j - 1);
        }
        if (i - 1 >= 0 && j + 1 < board.getN()) {
            clearCell(i - 1, j + 1);
        }
        if (i - 1 >= 0 && j - 1 >= 0) {
            clearCell(i - 1, j - 1);
        }
    }

    private boolean checkSetRook(int k, int l) {
        for (int i = 0; i < board.getM(); i++) {
            if (i != k) {
                if (!checkCellValue(i, l)) {
                    return false;
                }
            }
        }
        for (int i = 0; i < board.getN(); i++) {
            if (i != l) {
                if (!checkCellValue(k, i)) {
                    return false;
                }
            }
        }
        return true;
    }

    private void setRook(int k, int l) {
        board.getTable()[k][l] = ROOK_INT;
        for (int i = 0; i < board.getM(); i++) {
            if (i != k) {
                setCellValue(i, l);
            }
        }
        for (int i = 0; i < board.getN(); i++) {
            if (i != l) {
                setCellValue(k, i);
            }
        }
    }

    private void removeRook(int k, int l) {
        board.getTable()[k][l] = Board.DEFAULT_SIGN;
        for (int i = 0; i < board.getM(); i++) {
            if (i != k) {
                clearCell(i, l);
            }
        }
        for (int i = 0; i < board.getN(); i++) {
            if (i != l) {
                clearCell(k, i);
            }
        }
    }

    private boolean checkSetKnight(int i, int j) {
        if (i + 1 < board.getM() && j + 2 < board.getN()) {
            if (!checkCellValue(i + 1, j + 2)) {
                return false;
            }
        }
        if (i + 2 < board.getM() && j + 1 < board.getN()) {
            if (!checkCellValue(i + 2, j + 1)) {
                return false;
            }
        }
        if (i + 2 < board.getM() && j - 1 >= 0) {
            if (!checkCellValue(i + 2, j - 1)) {
                return false;
            }
        }
        if (i + 1 < board.getM() && j - 2 >= 0) {
            if (!checkCellValue(i + 1, j - 2)) {
                return false;
            }
        }
        if (i - 1 >= 0 && j - 2 >= 0) {
            if (!checkCellValue(i - 1, j - 2)) {
                return false;
            }
        }
        if (i - 2 >= 0 && j - 1 >= 0) {
            if (!checkCellValue(i - 2, j - 1)) {
                return false;
            }
        }
        if (i - 2 >= 0 && j + 1 < board.getN()) {
            if (!checkCellValue(i - 2, j + 1)) {
                return false;
            }
        }
        if (i - 1 >= 0 && j + 2 < board.getN()) {
            if (!checkCellValue(i - 1, j + 2)) {
                return false;
            }
        }
        return true;
    }

    private void setKnight(int i, int j) {
        board.getTable()[i][j] = KNIGHT_INT;
        if (i + 1 < board.getM() && j + 2 < board.getN()) {
            setCellValue(i + 1, j + 2);
        }
        if (i + 2 < board.getM() && j + 1 < board.getN()) {
            setCellValue(i + 2, j + 1);
        }
        if (i + 2 < board.getM() && j - 1 >= 0) {
            setCellValue(i + 2, j - 1);
        }
        if (i + 1 < board.getM() && j - 2 >= 0) {
            setCellValue(i + 1, j - 2);
        }
        if (i - 1 >= 0 && j - 2 >= 0) {
            setCellValue(i - 1, j - 2);
        }
        if (i - 2 >= 0 && j - 1 >= 0) {
            setCellValue(i - 2, j - 1);
        }
        if (i - 2 >= 0 && j + 1 < board.getN()) {
            setCellValue(i - 2, j + 1);
        }
        if (i - 1 >= 0 && j + 2 < board.getN()) {
            setCellValue(i - 1, j + 2);
        }
    }

    private void removeKnight(int i, int j) {
        board.getTable()[i][j] = Board.DEFAULT_SIGN;
        if (i + 1 < board.getM() && j + 2 < board.getN()) {
            clearCell(i + 1, j + 2);
        }
        if (i + 2 < board.getM() && j + 1 < board.getN()) {
            clearCell(i + 2, j + 1);
        }
        if (i + 2 < board.getM() && j - 1 >= 0) {
            clearCell(i + 2, j - 1);
        }
        if (i + 1 < board.getM() && j - 2 >= 0) {
            clearCell(i + 1, j - 2);
        }
        if (i - 1 >= 0 && j - 2 >= 0) {
            clearCell(i - 1, j - 2);
        }
        if (i - 2 >= 0 && j - 1 >= 0) {
            clearCell(i - 2, j - 1);
        }
        if (i - 2 >= 0 && j + 1 < board.getN()) {
            clearCell(i - 2, j + 1);
        }
        if (i - 1 >= 0 && j + 2 < board.getN()) {
            clearCell(i - 1, j + 2);
        }
    }

    private void findCombinations(byte[] figuresList) {
        int f = 0;
        int startI = 0;
        int startJ = 0;
        byte[] currentFigures = new byte[figuresList.length];
        int[][] figuresXY = new int[figuresList.length][2];
        System.arraycopy(figuresList, 0, currentFigures, 0, figuresList.length);
        while (f < figuresList.length && startJ < board.getN() && startI < board.getM()) {
            int i = startI;
            while (i < board.getM() && f < currentFigures.length) {
                int j = startJ;
                while (j < board.getN()) {
                    if (board.getTable()[i][j] == Board.DEFAULT_SIGN && f < currentFigures.length) {
                        switch (currentFigures[f]) {
                            case KING_INT: {
                                if (checkSetKing(i, j)) {
                                    setKing(i, j);
                                    currentFigures[f] = Board.DEFAULT_SIGN;
                                    figuresXY[f][0] = i;
                                    figuresXY[f][1] = j;
                                    f++;
                                }
                                break;
                            }
                            case ROOK_INT: {
                                if (checkSetRook(i, j)) {
                                    setRook(i, j);
                                    currentFigures[f] = Board.DEFAULT_SIGN;
                                    figuresXY[f][0] = i;
                                    figuresXY[f][1] = j;
                                    f++;
                                }
                                break;
                            }
                            case KNIGHT_INT: {
                                if (checkSetKnight(i, j)) {
                                    setKnight(i, j);
                                    currentFigures[f] = Board.DEFAULT_SIGN;
                                    figuresXY[f][0] = i;
                                    figuresXY[f][1] = j;
                                    f++;
                                }
                                break;
                            }
                        }
                    }
                    j++;
                }
                i++;
                startJ = 0;
            }
            startJ++;
            if (startJ == board.getN()) {
                startI++;
                startJ = 0;
            }
            if (checkFiguresSet(currentFigures)) {
                String[][] result = prepareForDisplay();
                if (findEqualsArrays(resultSet, result)) {
                    resultSet.add(result);
                }
                board.emptyBoard();
                if (f == figuresList.length && figuresXY[f - 1][0] < board.getM() &&  figuresXY[f - 1][1] < board.getN()) {
                    startI = figuresXY[0][0];     //TODO refactor repeat code
                    startJ = figuresXY[0][1];
                    startJ++;
                    if (startJ == board.getN()) {
                        startI++;
                        startJ = 0;
                    }
                    f = 0;
                    System.arraycopy(figuresList, 0, currentFigures, 0, figuresList.length);
                    figuresXY = new int[figuresList.length][2];
                } else if (f < figuresList.length) {
                    f = 0;
                    System.arraycopy(figuresList, 0, currentFigures, 0, figuresList.length);
                    figuresXY = new int[figuresList.length][2];
                }
            } else {
                if (f > 0) {
                    f--;
                    startI = figuresXY[f][0];
                    startJ = figuresXY[f][1];
                    if (figuresList[f] == KING_INT) {
                        removeKing(startI, startJ);
                    } else if (figuresList[f] == ROOK_INT) {
                        removeRook(startI, startJ);
                    } else if (figuresList[f] == KNIGHT_INT) {
                        removeKnight(startI, startJ);
                    }

                    if (startI == board.getM() - 1 && startJ == board.getN() - 1 && !checkFiguresSet(currentFigures) && f > 0) {
                        f--;      //TODO refactor out of the loop
                        startI = figuresXY[f][0];
                        startJ = figuresXY[f][1];
                        if (figuresList[f] == KING_INT) {
                            removeKing(startI, startJ);
                        } else if (figuresList[f] == ROOK_INT) {
                            removeRook(startI, startJ);
                        } else if (figuresList[f] == KNIGHT_INT) {
                            removeKnight(startI, startJ);
                        }
                        System.arraycopy(figuresList, 0, currentFigures, 0, figuresList.length);
                    } else {
                        currentFigures[f] = figuresList[f];
                    }

                    if (startJ + 1 == board.getN() && startI + 1 < board.getM()) {
                        startJ = 0;
                        startI++;
                    } else {
                        startJ++;
                    }

                    figuresXY[f][0] = 0;
                    figuresXY[f][1] = 0;
                }
            }
        }
    }

    private boolean compareArrays(String[][] combination, String[][] combinationForCheck) {
        for (int i = 0; i < combination.length; i++) {
            for (int j = 0; j < combination[i].length; j++) {
                if (!combination[i][j].equals(combinationForCheck[i][j])) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean findEqualsArrays(List<String[][]> combinations, String[][] combination) {
        boolean result = true;
        for (String[][] array : combinations) {
            if (compareArrays(array, combination)) {
                result = false;
            }
        }
        return result;
    }

    public List<String[][]> countResultSets() {
        long startTime = System.currentTimeMillis();
        resultSet = new ArrayList<>();

        permuteFigures(figures, 0);
        figuresCombinations.forEach(this::findCombinations);

        printResult();
        long endTime = System.currentTimeMillis();
        System.out.printf("Time for calculating: %s ms", endTime - startTime);
        return resultSet;
    }

    private boolean checkFiguresSet(byte[] array) {
        for (byte element : array) {
            if (element != Board.DEFAULT_SIGN) {
                return false;
            }
        }
        return true;
    }

    private String[][] prepareForDisplay() {
        String[][] result = new String[board.getM()][board.getN()];
        for (int i = 0; i < board.getM(); i++) {
            for (int j = 0; j < board.getN(); j++) {
                switch (board.getTable()[i][j]) {
                    case KING_INT: {
                        result[i][j] = KING;
                        break;
                    }
                    case ROOK_INT: {
                        result[i][j] = ROOK;
                        break;
                    }
                    case KNIGHT_INT: {
                        result[i][j] = KNIGHT;
                        break;
                    }
                    default: {
                        result[i][j] = EMPTY_SIGN;
                    }
                }
            }
        }
        return result;
    }

    private void printTable(String[][] table) {
        for (String[] string : table) {
            for (String value : string) {
                System.out.printf("%s ", value);
            }
            System.out.println();
        }
        System.out.println();
    }

    private void printResult() {
        if (!resultSet.isEmpty()) {
            for (String[][] array : resultSet) {
                for (String[] string : array) {
                    for (String value : string) {
                        System.out.printf("%s ", value);
                    }
                    System.out.println();
                }
                System.out.println();
            }
            System.out.printf("Found %s combinations with figures %s", resultSet.size(),
                    Arrays.asList(figuresString));
        } else {
            System.out.printf("There is no combinations with figures %s on board %s x %s",
                    Arrays.asList(figuresString), board.getN(), board.getM());
        }
        System.out.println();
    }

}
