/**
 * Created by lapanalda on 13.06.15.
 */
public class Board {

    public static final byte DEFAULT_SIGN = -1;

    private int m;

    private int n;

    private byte[][] table;

    public Board(int m, int n) {
        this.m = m;
        this.n = n;
        this.emptyBoard();
    }

    public void emptyBoard() {
        this.table = new byte[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                this.table[i][j] = DEFAULT_SIGN;
            }
        }
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public byte[][] getTable() {
        return table;
    }
}
